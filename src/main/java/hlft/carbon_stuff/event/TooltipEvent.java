package hlft.carbon_stuff.event;

import hlft.carbon_stuff.index.CSItems;
import hlft.carbon_stuff.item.instance.BlazingCoalCompoundItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import static hlft.carbon_stuff.util.TTU.*;

@Mod.EventBusSubscriber
public class TooltipEvent {
    @SubscribeEvent
    public static void addItemTooltip(ItemTooltipEvent event) {
        ItemStack stack = event.getItemStack();
        Item item = stack.getItem();
        if (item instanceof BlazingCoalCompoundItem) {
            if (((BlazingCoalCompoundItem) item).getState(stack) == 1){
                addToolTip(event, CSItems.BLAZING_COAL_COMPOUND, TextFormatting.BLUE);
            }else
                addToolTip(event, CSItems.BLAZING_COAL_COMPOUND, TextFormatting.GOLD);
        }
    }
}
