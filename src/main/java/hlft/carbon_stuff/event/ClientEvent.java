package hlft.carbon_stuff.event;

import com.simibubi.create.repack.registrate.util.entry.BlockEntry;
import hlft.carbon_stuff.index.CSItems;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

import static hlft.carbon_stuff.util.ORU.*;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEvent {
    @SubscribeEvent
    public static void overrideRegistry(FMLClientSetupEvent event) {
        addPolymorphicOverride(event, CSItems.BLAZING_COAL_COMPOUND);
        addBowOverride(event, CSItems.GRAPHITE_BOW);
        addCrossbowOverride(event, CSItems.GRAPHITE_CROSSBOW);
    }

    @SubscribeEvent
    public static void onRenderTypeSetup(FMLClientSetupEvent event) {
    }

    private static void addRenderLayer(FMLClientSetupEvent event, BlockEntry<?> block, RenderType type) {
        event.enqueueWork(() -> RenderTypeLookup.setRenderLayer(block.get(), type));
    }
}
