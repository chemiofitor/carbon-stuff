package hlft.carbon_stuff.tile.renderer;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.simibubi.create.content.logistics.block.depot.DepotBehaviour;
import com.simibubi.create.content.logistics.block.depot.DepotTileEntity;
import com.simibubi.create.foundation.tileEntity.renderer.SafeTileEntityRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3f;

public class SuspensionTableRenderer extends SafeTileEntityRenderer<DepotTileEntity> {
    public SuspensionTableRenderer(TileEntityRendererDispatcher dispatcher) {
        super(dispatcher);
    }

    @Override
    protected void renderSafe(DepotTileEntity te, float v, MatrixStack ms, IRenderTypeBuffer buffer, int light, int overlay) {
        DepotBehaviour db = te.getBehaviour(DepotBehaviour.TYPE);
        ItemRenderer itemRenderer = Minecraft.getInstance().getItemRenderer();
        ItemStack stack = db.getHeldItemStack();
        IBakedModel ibakedmodel = itemRenderer.getModel(stack, te.getWorld(), null);
        ms.pushPose();
        ms.scale(0.5f,0.5f,0.5f);
        ms.translate(1, 2.4, 1);
        double tick = System.currentTimeMillis() / 800.0D;
        ms.translate(0.0D, Math.sin(tick % (2 * Math.PI)) * 0.065D, 0.0D);
        ms.mulPose(Vector3f.YP.rotationDegrees((float) ((tick * 40.0D) % 360)));
        itemRenderer.render(stack, ItemCameraTransforms.TransformType.FIXED, true, ms, buffer, light, overlay, ibakedmodel);
        ms.popPose();
    }
}
