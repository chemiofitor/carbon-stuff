package hlft.carbon_stuff.item.instance;

import hlft.carbon_stuff.util.SUU;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.Rarity;

public class GraphiteCrossbow extends CrossbowItem {
    public GraphiteCrossbow(Properties properties) {
        super(properties.fireResistant().rarity(Rarity.RARE).defaultDurability(SUU.GRAPHITE_TIER.getUses()));
    }
}
