package hlft.carbon_stuff.item.instance;

import hlft.carbon_stuff.util.SUU;
import net.minecraft.item.*;

public class GraphiteTool {
    public static class GraphiteSword extends SwordItem {
        public GraphiteSword(Properties properties) {
            super(SUU.GRAPHITE_TIER, 3, -2.4F, properties.fireResistant());
        }
    }

    public static class GraphitePickaxe extends PickaxeItem {
        public GraphitePickaxe(Properties properties) {
            super(SUU.GRAPHITE_TIER, 1, -2.8F, properties.fireResistant());
        }
    }

    public static class GraphiteAxe extends AxeItem {
        public GraphiteAxe(Properties properties) {
            super(SUU.GRAPHITE_TIER, 5, -3.0F, properties.fireResistant());
        }
    }

    public static class GraphiteShovel extends ShovelItem {
        public GraphiteShovel(Properties properties) {
            super(SUU.GRAPHITE_TIER, 1.5F, -3.0F, properties.fireResistant());
        }
    }

    public static class GraphiteHoe extends HoeItem {
        public GraphiteHoe(Properties properties) {
            super(SUU.GRAPHITE_TIER, -4, 0.0F, properties.fireResistant());
        }
    }
}
