package hlft.carbon_stuff.item.instance;

import com.simibubi.create.content.curiosities.CombustibleItem;
import hlft.carbon_stuff.item.PolymorphicItem;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlazingCoalCompoundItem extends CombustibleItem implements PolymorphicItem {
    public BlazingCoalCompoundItem(Properties properties) {
        super(properties);
    }

    @Override
    public ActionResultType useOn(ItemUseContext context) {
        World world = context.getLevel();
        BlockPos pos = context.getClickedPos();
        Block block = world.getBlockState(pos).getBlock();
        ItemStack stack = context.getItemInHand();
        if (block.is(Blocks.FIRE)) {
            this.setState(stack, 0);
            world.playSound(context.getPlayer(), pos, SoundEvents.BLAZE_SHOOT, SoundCategory.HOSTILE, 2.0F, 1.0F);
            return ActionResultType.SUCCESS;
        }else if (block.is(Blocks.SOUL_FIRE)) {
            this.setState(stack, 1);
            world.playSound(context.getPlayer(), pos, SoundEvents.BLAZE_SHOOT, SoundCategory.HOSTILE, 2.0F, 1.0F);
            return ActionResultType.SUCCESS;
        }else
            return ActionResultType.PASS;
    }
}
