package hlft.carbon_stuff.item.groups;

import hlft.carbon_stuff.index.CSItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public class ModGroup extends ItemGroup {
    public static ModGroup MAIN;

    public ModGroup(String label) {
        super(label);
        MAIN = this;
    }

    @Nonnull
    @Override
    public ItemStack makeIcon() {
        return CSItems.GRAPHITE.asStack();
    }
}
