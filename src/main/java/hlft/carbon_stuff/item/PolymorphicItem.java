package hlft.carbon_stuff.item;

import net.minecraft.item.ItemStack;

public interface PolymorphicItem {
    default int getState(ItemStack itemStack) {
        return itemStack.getOrCreateTag().getInt("state");
    }

    default void setState(ItemStack itemStack, int data) {
        itemStack.getOrCreateTag().putInt("state", data);
    }
}
