package hlft.carbon_stuff;

import com.simibubi.create.foundation.data.CreateRegistrate;
import com.simibubi.create.repack.registrate.util.NonNullLazyValue;
import hlft.carbon_stuff.index.CSBlocks;
import hlft.carbon_stuff.index.CSItems;
import hlft.carbon_stuff.index.CSTileEntities;
import hlft.carbon_stuff.item.groups.ModGroup;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

@Mod("carbon_stuff")
public class CarbonStuffMod {
    public static String MODID = "carbon_stuff";

    private static final NonNullLazyValue<CreateRegistrate> registrate = CreateRegistrate.lazy(MODID);

    public CarbonStuffMod() {
        new ModGroup(MODID);

        MinecraftForge.EVENT_BUS.register(this);

        CSTileEntities.register();
        CSBlocks.register();
        CSItems.register();

    }

    public static CreateRegistrate registrate() {
        return registrate.get();
    }
}
