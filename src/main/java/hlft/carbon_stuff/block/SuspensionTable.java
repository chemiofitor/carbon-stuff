package hlft.carbon_stuff.block;

import com.simibubi.create.content.logistics.block.depot.DepotTileEntity;
import com.simibubi.create.content.logistics.block.depot.SharedDepotBlockMethods;
import com.simibubi.create.foundation.block.ITE;
import hlft.carbon_stuff.index.CSTileEntities;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.pathfinding.PathType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class SuspensionTable extends Block implements ITE<DepotTileEntity> {

    public SuspensionTable(Properties properties) {
        super(properties);
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        VoxelShape shape1 = Block.box(1, 0, 1, 15, 1, 15);
        VoxelShape shape2 = Block.box(2, 1, 2, 14, 2, 14);
        VoxelShape shape3 = Block.box(3, 2, 3, 13, 12, 13);
        VoxelShape shape4 = Block.box(2, 12, 2, 14, 13, 14);
        VoxelShape shape5 = Block.box(1, 13, 1, 15, 14, 15);
        VoxelShape shape6 = Block.box(2, 14, 2, 14, 15, 14);
        return VoxelShapes.or(shape1,shape2,shape3,shape4,shape5,shape6);
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return CSTileEntities.SUSPENSION_TABLE_TILE_ENTITY.create();
    }

    @Override
    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult ray) {
        return SharedDepotBlockMethods.onUse(state, world, pos, player, hand, ray);
    }

    @Override
    public void onRemove(BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving) {
        SharedDepotBlockMethods.onReplaced(state, world, pos, newState, isMoving);
    }

    @Override
    public void updateEntityAfterFallOn(IBlockReader worldIn, Entity entityIn) {
        super.updateEntityAfterFallOn(worldIn, entityIn);
        SharedDepotBlockMethods.onLanded(worldIn, entityIn);
    }

    @Override
    public Class<DepotTileEntity> getTileEntityClass() {
        return DepotTileEntity.class;
    }

    @Override
    public boolean hasAnalogOutputSignal(BlockState state) {
        return true;
    }

    @Override
    public boolean isPathfindable(BlockState state, IBlockReader reader, BlockPos pos, PathType type) {
        return false;
    }

    @Override
    public int getAnalogOutputSignal(BlockState blockState, World worldIn, BlockPos pos) {
        return SharedDepotBlockMethods.getComparatorInputOverride(blockState, worldIn, pos);
    }
}
