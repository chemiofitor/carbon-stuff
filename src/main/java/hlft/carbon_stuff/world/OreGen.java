package hlft.carbon_stuff.world;

import hlft.carbon_stuff.index.CSBlocks;
import hlft.carbon_stuff.util.RLU;
import net.minecraft.block.Block;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = "carbon_stuff")
public class OreGen {
    public static ConfiguredFeature<?,?> OreGraphite;

    public static void ores() {
        OreGraphite = Feature.ORE.configured(createOreConfig(FillType.NATURAL_STONE, CSBlocks.GRAPHITE_ORE.get(), 5))
                .range(75)
                .squared()
                .count(10);

        Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, RLU.modRL("ore_graphite"), OreGraphite);
    }

    @SubscribeEvent
    public static void onBiomeLoading(BiomeLoadingEvent event) {
        ores();
        event.getGeneration().getFeatures(GenerationStage.Decoration.UNDERGROUND_ORES).add(() -> OreGraphite);
    }

    private static OreFeatureConfig createOreConfig(FillType fillType, Block block, int count) {
        if (fillType == FillType.NATURAL_STONE) {
            return new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NATURAL_STONE, block.defaultBlockState(), count);
        }else if (fillType == FillType.NETHERRACK) {
            return new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NETHERRACK, block.defaultBlockState(), count);
        }else {
            return new OreFeatureConfig(OreFeatureConfig.FillerBlockType.NETHER_ORE_REPLACEABLES, block.defaultBlockState(), count);
        }
    }

    enum FillType{
        NATURAL_STONE,
        NETHERRACK,
        NETHER_ORE_REPLACEABLES
    }
}
