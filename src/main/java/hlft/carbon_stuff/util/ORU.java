package hlft.carbon_stuff.util;

import com.simibubi.create.repack.registrate.util.entry.ItemProviderEntry;
import hlft.carbon_stuff.item.PolymorphicItem;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

import static hlft.carbon_stuff.util.RLU.modRL;

public class ORU {
    public static void addBowOverride(FMLClientSetupEvent event, ItemProviderEntry<?> entry) {
        Item item = entry.get().asItem();
        addPullOverride(event, item);
        addPullingOverride(event, item);
    }

    public static void addCrossbowOverride(FMLClientSetupEvent event, ItemProviderEntry<? extends CrossbowItem> entry) {
        Item item = entry.get().asItem();
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pull"), (stack, world, entity) -> {
            if (entity == null) {
                return 0.0F;
            } else {
                return CrossbowItem.isCharged(stack) ? 0.0F : (float)(stack.getUseDuration() - entity.getUseItemRemainingTicks()) / (float)CrossbowItem.getChargeDuration(stack);
            }
        }));

        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pulling"), (stack, world, entity) ->
                entity != null && entity.isUsingItem() && entity.getUseItem() == stack && !CrossbowItem.isCharged(stack) ? 1.0F : 0.0F));

        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("charged"), (stack, world, entity) ->
                entity != null && CrossbowItem.isCharged(stack) ? 1.0F : 0.0F));

        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("firework"), (stack, world, entity) ->
                entity != null && CrossbowItem.isCharged(stack) && CrossbowItem.containsChargedProjectile(stack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F));
    }

    public static void addPullOverride(FMLClientSetupEvent event, Item item) {
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pull"), (stack, world, entity) -> {
            if (entity == null) {
                return 0.0F;
            } else {
                return entity.getUseItem() != stack ? 0.0F : (float)(stack.getUseDuration() - entity.getUseItemRemainingTicks()) / 20.0F;
            }
        }));
    }

    public static void addPullingOverride(FMLClientSetupEvent event, Item item) {
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pulling"), (stack, world, entity) ->
                entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F));
    }

    public static void addPolymorphicOverride(FMLClientSetupEvent event, ItemProviderEntry<? extends PolymorphicItem> item) {
        event.enqueueWork(() ->ItemModelsProperties.register(item.get().asItem(), modRL("state"), (stack, world, entity) ->
                stack.hasTag() ? item.get().getState(stack) : 0.0F));
    }
}
