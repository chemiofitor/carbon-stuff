package hlft.carbon_stuff.util;

import hlft.carbon_stuff.index.CSItems;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public class SUU {
    public static final IItemTier GRAPHITE_TIER =
            createTier(4, 2506, 11.0F, 4.5F, 6, CSItems.GRAPHITE.get());

    public static IItemTier createTier(int destroyLevel, int use, float destroySpeed, float baseAttackDamage, int EV, Item repairItem) {
        return new IItemTier() {
            @Override
            public int getUses() {
                return use;
            }

            @Override
            public float getSpeed() {
                return destroySpeed;
            }

            @Override
            public int getLevel() {
                return destroyLevel;
            }

            @Override
            public float getAttackDamageBonus() {
                return baseAttackDamage;
            }

            @Override
            public int getEnchantmentValue() {
                return EV;
            }

            @Override
            public Ingredient getRepairIngredient() {
                return Ingredient.of(repairItem.getDefaultInstance());
            }
        };
    }
}
