package hlft.carbon_stuff.util;

import com.simibubi.create.foundation.block.connected.CTSpriteShiftEntry;
import hlft.carbon_stuff.CarbonStuffMod;
import net.minecraft.util.ResourceLocation;

import static com.simibubi.create.foundation.block.connected.CTSpriteShifter.CTType.OMNIDIRECTIONAL;
import static com.simibubi.create.foundation.block.connected.CTSpriteShifter.getCT;

public class RLU {
    public static final CTSpriteShiftEntry CTS_GRAPHITE_CASING = createCT("graphite_casing");

    public static ResourceLocation modRL(String path) {
        return new ResourceLocation(CarbonStuffMod.MODID, path);
    }

    public static CTSpriteShiftEntry createCT(String path) {
        return getCT(OMNIDIRECTIONAL,  modRL("block/" + path), modRL("block/"+ path + "_connected"));
    }
}
