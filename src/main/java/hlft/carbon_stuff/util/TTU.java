package hlft.carbon_stuff.util;

import com.simibubi.create.repack.registrate.util.entry.ItemProviderEntry;
import net.minecraft.item.Item;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public class TTU {
    public static void addToolTip(ItemTooltipEvent event, ItemProviderEntry<?> entry) {
        Item item = entry.get().asItem();
        String description = item.getDescriptionId();
        addToolTip(event, item, new TranslationTextComponent(description + ".tooltip"));
    }

    public static void addToolTip(ItemTooltipEvent event, ItemProviderEntry<?> entry, TextFormatting color) {
        Item item = entry.get().asItem();
        String description = item.getDescriptionId();
        IFormattableTextComponent key = new TranslationTextComponent(description + ".tooltip").withStyle(color);
        addToolTip(event, item, (TextComponent) key);
    }

    public static void addToolTip(ItemTooltipEvent event, Item item, TextComponent text) {
        if (item == event.getItemStack().getItem()) {
            event.getToolTip().add(text);
        }
    }
}
