package hlft.carbon_stuff.index;

import com.simibubi.create.content.logistics.block.depot.DepotTileEntity;
import com.simibubi.create.foundation.data.CreateRegistrate;
import com.simibubi.create.repack.registrate.util.entry.TileEntityEntry;
import hlft.carbon_stuff.CarbonStuffMod;
import hlft.carbon_stuff.tile.renderer.SuspensionTableRenderer;

public class CSTileEntities {
    private static final CreateRegistrate REGISTRATE = CarbonStuffMod.registrate();

    public static final TileEntityEntry<DepotTileEntity> SUSPENSION_TABLE_TILE_ENTITY =
            REGISTRATE.tileEntity("depot", DepotTileEntity::new)
                    .validBlocks(CSBlocks.SUSPENSION_TABLE)
                    .renderer(() -> SuspensionTableRenderer::new)
                    .register();


    public static void register() {}
}
