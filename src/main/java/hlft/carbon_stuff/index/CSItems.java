package hlft.carbon_stuff.index;

import com.simibubi.create.content.curiosities.CombustibleItem;
import com.simibubi.create.foundation.data.CreateRegistrate;
import com.simibubi.create.repack.registrate.util.entry.ItemEntry;
import hlft.carbon_stuff.CarbonStuffMod;
import hlft.carbon_stuff.item.groups.ModGroup;
import hlft.carbon_stuff.item.instance.BlazingCoalCompoundItem;
import hlft.carbon_stuff.item.instance.GraphiteBow;
import hlft.carbon_stuff.item.instance.GraphiteCrossbow;
import hlft.carbon_stuff.item.instance.GraphiteTool;
import net.minecraft.item.Item;

public class CSItems {
    private static final CreateRegistrate REGISTRATE = CarbonStuffMod.registrate()
            .itemGroup(() -> ModGroup.MAIN);

    public static final ItemEntry<Item> GRAPHITE =
            REGISTRATE.item("graphite", Item::new)
                    .register();

    public static final ItemEntry<Item> GRAPHITE_COMPOUND =
            REGISTRATE.item("graphite_compound", Item::new)
                    .register();

    public static final ItemEntry<Item> GRAPHITE_PLATE =
            REGISTRATE.item("graphite_plate", Item::new)
                    .register();

    public static final ItemEntry<Item> GRAPHITE_DUST =
            REGISTRATE.item("graphite_dust", Item::new)
                    .register();

    public static final ItemEntry<CombustibleItem> CRUSHED_COAL =
            REGISTRATE.item("crushed_coal", CombustibleItem::new)
                    .onRegister(i -> i.setBurnTime(2000))
                    .register();

    public static final ItemEntry<CombustibleItem> BLAZING_COAL =
            REGISTRATE.item("blazing_coal", CombustibleItem::new)
                    .onRegister(i -> i.setBurnTime(5600))
                    .register();

    public static final ItemEntry<BlazingCoalCompoundItem> BLAZING_COAL_COMPOUND =
            REGISTRATE.item("blazing_coal_compound", BlazingCoalCompoundItem::new)
                    .onRegister(i -> i.setBurnTime(16000))
                    .register();

    public static final ItemEntry<Item> GRAPHITE_STICK =
            REGISTRATE.item("graphite_stick", Item::new)
                    .register();

    public static final ItemEntry<GraphiteTool.GraphiteSword> GRAPHITE_SWORD =
            REGISTRATE.item("graphite_sword", GraphiteTool.GraphiteSword::new)
                    .register();

    public static final ItemEntry<GraphiteTool.GraphitePickaxe> GRAPHITE_PICKAXE =
            REGISTRATE.item("graphite_pickaxe", GraphiteTool.GraphitePickaxe::new)
                    .register();

    public static final ItemEntry<GraphiteTool.GraphiteAxe> GRAPHITE_AXE =
            REGISTRATE.item("graphite_axe", GraphiteTool.GraphiteAxe::new)
                    .register();

    public static final ItemEntry<GraphiteTool.GraphiteShovel> GRAPHITE_SHOVEL =
            REGISTRATE.item("graphite_shovel", GraphiteTool.GraphiteShovel::new)
                    .register();

    public static final ItemEntry<GraphiteTool.GraphiteHoe> GRAPHITE_HOE =
            REGISTRATE.item("graphite_hoe", GraphiteTool.GraphiteHoe::new)
                    .register();

    public static final ItemEntry<GraphiteBow> GRAPHITE_BOW =
            REGISTRATE.item("graphite_bow", GraphiteBow::new)
                    .register();

    public static final ItemEntry<GraphiteCrossbow> GRAPHITE_CROSSBOW =
            REGISTRATE.item("graphite_crossbow", GraphiteCrossbow::new)
                    .register();

    public static final ItemEntry<Item> VISCOUS_STRING =
            REGISTRATE.item("viscous_string", Item::new)
                    .register();

    public static final ItemEntry<Item> CARBONIZED_VISCOUS_STRING =
            REGISTRATE.item("carbonized_viscous_string", Item::new)
                    .register();

    public static void register() {}
}
