package hlft.carbon_stuff.index;

import com.simibubi.create.content.contraptions.base.CasingBlock;
import com.simibubi.create.foundation.data.BuilderTransformers;
import com.simibubi.create.foundation.data.CreateRegistrate;
import com.simibubi.create.foundation.data.SharedProperties;
import com.simibubi.create.repack.registrate.util.entry.BlockEntry;
import hlft.carbon_stuff.CarbonStuffMod;
import hlft.carbon_stuff.block.SuspensionTable;
import hlft.carbon_stuff.item.groups.ModGroup;
import hlft.carbon_stuff.util.RLU;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraftforge.common.ToolType;

import static com.simibubi.create.foundation.data.ModelGen.customItemModel;

public class CSBlocks {
    private static final CreateRegistrate REGISTRATE = CarbonStuffMod.registrate()
            .itemGroup(() -> ModGroup.MAIN);

    public static final BlockEntry<Block> GRAPHITE_ORE =
            REGISTRATE.block("graphite_ore", Block::new)
                    .initialProperties(SharedProperties::stone)
                    .item()
                    .transform(customItemModel())
                    .properties((p) -> p.harvestLevel(1).harvestTool(ToolType.PICKAXE).sound(SoundType.STONE))
                    .register();

    public static final BlockEntry<Block> GRAPHITE_BLOCK =
            REGISTRATE.block("graphite_block", Block::new)
                    .initialProperties(() -> Blocks.IRON_BLOCK)
                    .item()
                    .transform(customItemModel())
                    .register();

    public static final BlockEntry<CasingBlock> GRAPHITE_CASING =
            REGISTRATE.block("graphite_casing", CasingBlock::new)
                    .transform(BuilderTransformers.casing(RLU.CTS_GRAPHITE_CASING))
                    .register();

    public static final BlockEntry<SuspensionTable> SUSPENSION_TABLE =
            REGISTRATE.block("suspension_table", SuspensionTable::new)
                    .initialProperties(() -> Blocks.IRON_BLOCK)
                    .item()
                    .transform(customItemModel())
                    .properties((p) -> p.harvestLevel(1).harvestTool(ToolType.PICKAXE).sound(SoundType.METAL))
                    .register();

    public static void register() {}
}
